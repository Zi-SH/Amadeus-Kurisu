# Import Base Modules
import sqlite3
import asyncio
import discord
import config
import modules.utils
import modules.responses
from discord.ext import commands
description = """Amadeus-Kurisu: Remade."""
command_prefix = config.command_prefix


startup_modules = ["modules.memes", "modules.admin", "modules.general", "modules.markov", "modules.announce", "modules.spoilers"]

loaded_modules = []

client = commands.Bot(command_prefix=command_prefix, description=description)
client.database = sqlite3.connect('kurisu.db')

client.cursor = client.database.cursor()

client.config = config

client.unmute_timers = {}

client.cursor.execute('''CREATE TABLE IF NOT EXISTS config (
    ServerID int, 
    ServerName varchar(255),
    Setting varchar(255),
    Value varchar(255)
    )
    ''')

client.cursor.execute('''CREATE TABLE IF NOT EXISTS mutes (

    UserID int,
    UserName varchar(255),
    MuteTime int,
    ServerID int

)
''')

client.database.commit()

#connection.close()

# Will be using a good chunk of Emoji's codebase for this. Way better than what I can come up with on my own.



@client.event
async def on_ready():
    """Readies the bot"""
    print("Logged in as")
    print(client.user.name)
    print(client.user.id)
    print("-------")
    #for server in client.servers:
        #server_id = (server.id)

    # CREATE TABLE IF NOT EXISTS logs-serverid

    for server in client.servers:
        client.unmute_timers.update({server.id: {}})
    
    for module in startup_modules:
        try:
            client.load_extension(module)
        except Exception as e:
            print("{} failed to load. \n{}: {}".format(module, type(e).__name__, e))

@client.command(pass_context=True)
async def load(context, extension_name: str):
    """Loads an extension"""
    if context.message.author.id == config.owner_id:
        try:
            client.load_extension(extension_name)
        except(AttributeError, ImportError) as error:
            await client.say("```py\n{}: {}\n```".format(type(error).__name__, str(error)))
            return
        await client.say("{} loaded.".format(extension_name))
        loaded_modules.append(extension_name)
    else:
        await client.say(modules.utils.randomize_string(modules.responses.no_permission_insults, {"%name%": context.message.author.mention}))

@client.command(pass_context=True, name="reload")
async def reload_module(context, extension_name: str = None):
    """Reloads a single or all extensions"""
    if context.message.author.id == config.owner_id:
        if extension_name is None:
            for module in loaded_modules:
                client.unload_extension(module)
                client.load_extension(module)
            await client.say("All modules reloaded.")
        else:
            client.unload_extension(extension_name)
            client.load_extension(extension_name)
            await client.say("{} reloaded.".format(extension_name))
    else:
        await client.say(modules.utils.randomize_string(modules.responses.no_permission_insults, {"%name%": context.message.author.mention}))


@client.command(pass_context=True)
async def unload(context, extension_name: str):
    """Unloads an extension"""
    if context.message.author.id == config.owner_id:
        client.unload_extension(extension_name)
        await client.say("{} unloaded.".format(extension_name))
        try:
            loaded_modules.remove(extension_name)
        except (ValueError, discord.ext.commands.CommandInvokeError):
            print("Didn't exist in list. Remove module anyway")
    else:
        await client.say(modules.utils.randomize_string(modules.responses.no_permission_insults, {"%name%": context.message.author.mention}))


@client.event
async def on_message(message):

    """Handles messages."""

    if message.author == client:
        return #Thanks Emoji. Haven't actually thought about that.

    await client.process_commands(message)




client.run(config.token)
client.database.close()
