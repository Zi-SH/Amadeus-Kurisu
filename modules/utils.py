from random import randrange
import modules.responses
import discord

# Insert Functions here. Outside of the class

import_client = ""

class utils:
	
	# Constructor

    def __init__(self, client):
        print("Addon {} loaded.".format(self.__class__.__name__))
        self.client = client
    

def randomize_string(string_array, replacements=None):
    number = randrange(0, len(string_array))
    print(number)
    responses = modules.responses

    picked_string = string_array[number]

    complete_string = picked_string

    if string_array == responses.invalid_argument_count:
        if replacements["%arguments_required%"] == "1":
            for keys, values in replacements.items():
                complete_string = complete_string.replace(keys,values)
            complete_string = complete_string.replace("arguments", "argument")
            return complete_string
        

    if replacements == None:
        return picked_string

    
    for keys, values in replacements.items():
        complete_string = complete_string.replace(keys, values)
    
    return complete_string




def fetch_news_channel(server, cursor, context):
    cursor.execute("SELECT * FROM config WHERE Setting='NewsChannel' AND ServerID=?", (context.message.channel.server.id,)) # Fetch newschannelid from server
    query = cursor.fetchone()
    #print(query)
    #print(value)

    #If the channel is defined in the database, proceed to check if the channel still exists, else use default channel
    if query != None: 
        print("Queryex")
        channel =  discord.utils.find(lambda r: r.id == str(query[3]), server.channels)
        if channel != None:
            print("Channelex")
            return channel
        else:
            return server.default_channel
    else:
        print("Defchan") 
        return server.default_channel
        

    


#Thanks PythonCentral

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        pass
 
    try:
        import unicodedata
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass
    return False

# Thanks Emoji

async def get_members(bot, msg, name: str):
    """
    This function is coroutine.
    
    Gets server member/members.
    Returns array of members in "Username#Discriminator" format/
    First member of this array (members[0]) should be passed to server.get_member_named() method.
    Members array can be used for similar results outputting.
    
    :param bot: Bot instance
    :param msg: Message
    :param name: Member name
    :return: Array
    """

    members = []

    # Search for a member by mention
    # First check if it's mention
    if name.startswith('<@'):
        print("Mention has been passed. Looking for the member...")
        name = name.strip('<@?!#$%^&*>')
        mem = msg.server.get_member(name)
        print("Member {} found!".format(mem.name))
        members.append(mem.name + '#' + mem.discriminator)
        return members
    else:
        # Search for a member with specific discriminator
        # Since username cannot contain hash we can safely split it
        if '#' in name:
            print("Name with discriminator has been passed. Looking for the member...")
            name_parts = name.split('#')
            for mem in msg.server.members:
                if name_parts[0].lower() in mem.name.lower() and name_parts[1] in mem.discriminator:
                    print("Member {} found!".format(mem.name))
                    members.append(mem.name + '#' + mem.discriminator)
                    # Since there can be only one specific member with this discriminator
                    # we can return members right after we found him
                    return members
            # If we didn't find any members with this discriminator then there's no point to continue.
            if not members:
                print("No members with this discriminator were found...")
                await bot.send_message(msg.channel, "Heh. That was honestly stupid, a mistake so big only a newfag would make it, after all, there is no such user. Lurk moar!")
                return None

        # Search for a member by username
        for mem in msg.server.members:
            # Limit number of results
            if name.lower() in mem.name.lower() and len(members) < 6:
                members.append(mem.name + '#' + mem.discriminator)

        # Search for a member by nickname
        # If we didn't find any members, then there is possibility that it's a nickname
        if not members:
            print("Members weren't found. Checking if it's a nickname...")
            for mem in msg.server.members:
                # Limit number of results & check if member has a nick and compare with input
                if mem.nick and name.lower() in mem.nick.lower() and len(members) < 6:
                    members.append(mem.name + '#' + mem.discriminator)

        # If no members this time, then return None and error message, else - return members
        if not members:
            print("No members were found")
            await bot.send_message(msg.channel, "Heh. That was honestly stupid, a mistake so big only a newfag would make it, after all, there is no such user. Lurk moar!")
            return None
        else:
            if len(members) > 1:
                await bot.say("You dummy! You weren't specific enough... There are multiple users like that! \n\n"
                                   "Lurk more before trying again, but just this once, here's a list of possible options for you:\n"
                                   "{}".format("\n".join(members)))
                return None
            else:
                return members






def setup(bot):
	bot.add_cog(utils())
