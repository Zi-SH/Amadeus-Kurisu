import codecs
from discord.ext import commands
import discord
import re


#def set_client(client_arg):
    #client = client_arg

class Spoilers:
    """This is a module for AmaKuri attempting to deal with spoilers"""

    def __init__(self, client):
        self.client = client
        print("Addon \"{}\" loaded.".format(self.__class__.__name__))
        #set_client(client)

        self.client.cursor.execute('''CREATE TABLE IF NOT EXISTS rot13consent (UserID int)''')

        self.client.database.commit()

    
    @commands.command(pass_context=True)
    async def rotrevoke(self, context):
        print("Entered")
        self.client.cursor.execute("SELECT * FROM rot13consent WHERE UserID=?", (context.message.author.id,))
        query = self.client.cursor.fetchall()
        if query:
            self.client.cursor.execute("DELETE FROM rot13consent WHERE UserID=?", (context.message.author.id,))
            self.client.database.commit()
            await self.client.say("You've cleared your consent!")
        else:
            print("Nuffin")


    async def on_message(self, message):
        if "rot13" in message.content.lower() or "ebg13" in message.content.lower():
            await self.client.add_reaction(message, "🔓")

        #await self.client.process_commands(message)

    async def on_reaction_add(self, reaction, user):

        if reaction.emoji == "🔓":
            #print("tbhdesune")
            #print(reaction.message.author.id)
            if "rot13" in reaction.message.content.lower() or "ebg13" in reaction.message.content.lower() or reaction.message.author.id == "128943467488477194" or reaction.message.author.id == "229341812890927106":
                #print("test")

                if user == self.client.user:
                    print("AmaKuri, do nuffin")
                    return

                self.client.cursor.execute("SELECT * FROM rot13consent WHERE UserID=?", (user.id,))
                query = self.client.cursor.fetchall()

                if query:
                    try:
                        author = "{} ({})".format(reaction.message.author, reaction.message.author.display_name) \
                            if reaction.message.author.nick \
                            else "{}".format(reaction.message.author)
                        rot_embed = discord.Embed(color=reaction.message.author.color)
                    except AttributeError as e:
                        print("Caught Error: " + str(e))
                        print("This is most likely because the user has left the server")
                        author = "{}".format(reaction.message.author)
                        rot_embed = discord.Embed(color=discord.Color.default())

                    if reaction.message.content:
                        rot_embed.add_field(name="Content:", value=codecs.encode(reaction.message.content.replace("rot13", ""), 'rot_13'), inline=False)

                    #rot_embed.set_image(url=str(reaction.message.attachments[0]['url']))
                    rot_embed.set_author(name=author, icon_url=reaction.message.author.avatar_url)

                    await self.client.send_message(user, embed=rot_embed)
                else:
                    await self.client.send_message(user, "Hey. It looks like it's your first time reacting to an unlock on a rot13'ed message. \nRot13 is a cypher used to hide spoilers on platforms that don't natively support spoiler tags, like Discord. \nJust to be sure that you agree to receiving spoilers upon reacting with the unlock emote, please reply with '!agree' within a minute of this being sent. \nYou can revoke this at any time by using the command !rotrevoke.")
                    consent = await self.client.wait_for_message(author=user, content='!agree', timeout=60)
                    if consent == None:
                        print("No consent given")
                    else:
                        consent_info = (user.id,) #If the query does in fact, not exist, just insert it instead of update.
                        self.client.cursor.execute("INSERT INTO rot13consent VALUES (?)", consent_info)
                        self.client.database.commit()

                        try:
                            author = "{} ({})".format(reaction.message.author, reaction.message.author.display_name) \
                                if reaction.message.author.nick \
                                else "{}".format(reaction.message.author)
                            rot_embed = discord.Embed(color=reaction.message.author.color)
                        except AttributeError as e:
                            print("Caught Error: " + str(e))
                            print("This is most likely because the user has left the server")
                            author = "{}".format(reaction.message.author)
                            rot_embed = discord.Embed(color=discord.Color.default())

                        if reaction.message.content:
                            rot_embed.add_field(name="Content:", value=codecs.encode(reaction.message.content.replace("rot13", ""), 'rot_13'), inline=False)

                        #rot_embed.set_image(url=str(reaction.message.attachments[0]['url']))
                        rot_embed.set_author(name=author, icon_url=reaction.message.author.avatar_url)

                        await self.client.send_message(user, embed=rot_embed)
                    


            
        #print("Event initiated")















def setup(client):
	client.add_cog(Spoilers(client))







#print(codecs.encode('Jeg kan godt lide kage læåø', 'rot_13'))