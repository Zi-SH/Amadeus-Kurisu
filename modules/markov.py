from discord.ext import commands
import sqlite3
import random
import re

class Markov:
    """This is the Markov module of Amadeus-Kurisu. This module's sole purpose is to handle the Markov chain function of Amadeus-Kurisu."""

    def __init__(self, client):
        self.client = client
        print("Addon \"{}\" loaded.".format(self.__class__.__name__))


    @commands.command(pass_context=True)
    async def test_log(self, context):
        randomized_int = random.randint(1, 602)
        with open("modules/markov/markov ({}).txt".format(randomized_int)) as file:
            word_dictionary = self.learn(file.read())
            print(word_dictionary)

    @commands.command(pass_context=True, aliases=['mahokov'])
    async def maholog(self, context):
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='maholog' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        #print(disabled_state[3])
        if disabled_state != None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            await self.markovGen(True)



    @commands.command(pass_context=True)
    async def markov(self, context):
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='markov' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        #print(disabled_state[3])
        if disabled_state != None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            await self.markovGen()





    
        
    @commands.command(pass_context=True)
    async def log(self, context):
        """!log Are you still drunk Kuri? -> times are bad :picardy:
        
        
        Generates a random message based based off logs in 602 files (~100 KB each) using Markov chaining.
        """
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='log' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        #print(disabled_state[3])
        if disabled_state != None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
          await self.markovGen()


    async def markovGen(self, maho=False):
            result = ""
            randomized_int = 0
            path = "bleh"
            if maho:
                randomized_int = random.randint(1, 61)
                path = "modules/Maholog/{}".format(randomized_int)
            else:
                randomized_int = random.randint(1, 602)
                path = "modules/markov/markov ({}).txt".format(randomized_int)

            with open(path) as file:
                #return returnable_file
                length = 50
                #print(file.name)
                word_dictionary = self.learn(file.read())
                punctuation = False
                last_word = "~~~~~~~~~~~~~~~~"

                while not punctuation:
                    new_word = self.get_next_word(last_word, word_dictionary).rstrip()
                    result = result + " " + new_word
                    result.replace("\r\n", '')
                    last_word = new_word

                    if len(result.split(" ")) > random.randint(3,8) and any(punct in result[-2:] for punct in ["!", ".", "?"]):
                        punctuation = True
            await self.client.say(result)


    def learn(self, input):  
        dict = {}
        word_tokens = re.split(" |\n", input)
        #word_tokens = [x.strip('\r\n').strip('\n') for x in input.split(" ")]

        for i in range(0, len(word_tokens)-1):
            current_word = word_tokens[i]
            next_word = word_tokens[i+1]

            if current_word not in dict:
                # Create new entry in dictionary
                dict[current_word] = {next_word : 1}
            else:
                # Current word already exists
                all_next_words = dict[current_word]

                if next_word not in all_next_words:
                    # Add new next state (word)
                    dict[current_word][next_word] = 1
                else:
                    # Already exists, just increment
                    dict[current_word][next_word] = dict[current_word][next_word] + 1

        return dict
    
    def get_next_word(self, last_word, dict):
        if last_word not in dict:
            # Random
            new_word = self.pick_random(dict)
            return new_word
        else:
            # Pick next word from list
            candidates = dict[last_word]
            candidates_normalised = []

            for word in candidates:
                freq = candidates[word]
                for i in range(0, freq):
                    candidates_normalised.append(word)

            rnd = random.randint(0, len(candidates_normalised)-1)
            return candidates_normalised[rnd]


    def pick_random(self, dict):
        random_num = random.randint(0, len(dict)-1)
        new_word = list(dict.keys())[random_num]
        return new_word

    

def setup(client):
    client.add_cog(Markov(client))