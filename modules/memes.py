from discord.ext import commands
import sqlite3
import modules.utils
import modules.responses
from random import randrange
import discord
import config


class Memes:
    """This is the Memes module of Amadeus-Kurisu. This module contains all of the memes for Amadeus-Kurisu."""

    

    def __init__(self, client):
        self.client = client
        self.utils = modules.utils
        self.responses = modules.responses
        print("Addon \"{}\" loaded.".format(self.__class__.__name__))
        

    @commands.command(pass_context=True, aliases=["nurupo"])
    async def nullpo(self, context):
        """I-I swear! I--I don't know that meme!
        
        """
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='nullpo' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ##print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            #print(self.utils.randomize_string(self.responses.lenny_replies))
            await self.client.say("Gah!")

    @commands.command(pass_context=True)
    async def robots(self, context):
        """Huh, what did you say, senpai? Eh-!? Robots!?

        Posts a Robotics;Notes meme to chat from a server.

        Command can be disabled.
        
        """
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='robots' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ##print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            await self.client.say("Coming soon:tm:.")

    @commands.command(pass_context=True)
    async def nosememes(self, context):
        """Th-that should teach that bastard EPK to shut up...! Shit memes should just be purged honestly...
        
        The bot posts if you use it.

        Command can be disabled. """
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='nosememes' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ##print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            await self.client.say(self.responses.ghost_lenny_replies[0])

    @commands.command(pass_context=True, aliases=["rip"])
    async def ripme(self, context, *, arg):
        """RIP You.
        
        

        Command can be disabled.
        """
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='ripme' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ##print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            url = "http://ripme.xyz/#" + arg.replace(" ", "%20")
            await self.client.say(url)
    

    @commands.command(pass_context=True)
    async def lenny(self, context):
        """Senpai, a-are you implying what I think you are?
        
        Posts a lenny to chat.

        Command can be disabled.
        
        """
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='lenny' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ##print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            print(self.utils.randomize_string(self.responses.lenny_replies))
            await self.client.say(self.utils.randomize_string(self.responses.lenny_replies))

    
    @commands.command(pass_context=True)
    async def ohmy(self, context):
        """Oh my, senpai you really have... <your good taste prevents you from reading the rest of the description>
        
        Posts an "Oh my..." to chat.

        Command can be disabled.

        """
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='ohmy' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ###print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            await self.client.say(self.utils.randomize_string(self.responses.ohmy_responses))
    
    @commands.group(pass_context=True)
    async def meme(self, context):
        """LUL MEMES AMIRITE?
        
        Pulls a meme from the database and posts it in chat.

        Command can be disabled.
        
        """
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='meme' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ####print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            self.client.cursor.execute("SELECT * FROM sqlite_master WHERE type='table' AND name='memes'") # Fetch current pingmessage from server

            query = self.client.cursor.fetchone()

            if query == None: 
                self.client.cursor.execute('''CREATE TABLE IF NOT EXISTS memes (
                    Name varchar(255),
                    Link varchar(255)
                    )
                    ''')
                self.client.database.commit()
            

            if context.invoked_subcommand is None:
                


                args = context.message.content.split("meme ")
                print(len(args))

                if len(args) is 1:
                    await self.client.say(self.utils.randomize_string(self.responses.invalid_argument_count, {"%name%": context.message.author.mention, "%arguments_required%": "1", "%arguments_used%": str(len(args)-1)}))
                else:
                    # await self.client.say("Logic for this coming soon.  Requested Meme: " + str(args[1]))
                    self.client.cursor.execute("SELECT * FROM memes WHERE name=? COLLATE NOCASE", (args[1].strip().lower(),))
                    
                    meme_query = self.client.cursor.fetchone()

                    print(meme_query)

                    if meme_query is None:
                        await self.client.say("Meme `" + args[1] + "` not found.")
                    else:
                        await self.client.say(meme_query[1])
                
                
    @meme.command(pass_context=True)
    async def add(self, context, name, content=None):
        """Adds a new meme to the database. Command gets disabled together with meme"""
        url = content
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='meme' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ##print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            print(self.client.config.owner_id)
            if context.message.author.id == self.client.config.owner_id:
                args_used = len(context.message.content.split("add ")[1].split(" "))
                print(args_used)
                #if args_used is not 2 and " " not in name:
                #    await self.client.say(self.utils.randomize_string(self.responses.invalid_argument_count, {"%name%": context.message.author.mention, "%arguments_required%": "2", "%arguments_used%": str(args_used)}))
                #else: 
                print(name)
                self.client.cursor.execute("SELECT * FROM memes WHERE name=?", (name,))

                add_query = self.client.cursor.fetchone()

                    

                if add_query is not None:
                    await self.client.say("Meme `" + name + "` already exists.")
                else:
                    #await self.client.say("Logic for this coming soon. Name: " + str(name) + " URL: " + str(url))
                    #print(query)
                    self.client.cursor.execute("INSERT INTO memes VALUES (?,?)", (name, url,))
                    self.client.database.commit()
                    await self.client.say("Meme `" + name + "` added with content of `" + url + "`")
            else:
                await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))
        
    @meme.command(pass_context=True, name="list")
    async def list_memes(self, context):
        """Lists memes from the database. Command gets disabled together with meme"""
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='meme' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ##print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            self.client.cursor.execute("SELECT * FROM memes")
            memes = self.client.cursor.fetchall()
            meme_string = """"""
            for meme in memes:
                meme_string += meme[0] + " \n"
            

            await self.client.say("```List of Memes: \n" + meme_string + "```")

    #@meme.command(pass_context=True, name=("rm","del","delete", "remove"))
    @meme.command(pass_context=True)
    async def remove(self, context, name):
        """Removes a meme from the database. Command gets disabled together with meme"""
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='meme' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ##print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled" :
            print("Command Disabled")
        else:
            print(self.client.config.owner_id)
            if context.message.author.id == self.client.config.owner_id:
                args_used = len(context.message.content.split("remove ")[1].split(" "))
                print(args_used)
                if args_used is not 1 and " " not in name:
                    await self.client.say(self.utils.randomize_string(self.responses.invalid_argument_count, {"%name%": context.message.author.mention, "%arguments_required%": "1", "%arguments_used%": str(args_used)}))
                else: 
                    print(name)
                    self.client.cursor.execute("SELECT * FROM memes WHERE name=?", (name,))

                    remove_query = self.client.cursor.fetchone()

                    

                    if remove_query is None:
                        await self.client.say("Meme `" + name + "` doesn't exist.")
                    else:
                        #await self.client.say("Logic for this coming soon. Name: " + str(name) + " URL: " + str(url))
                        #print(query)
                        self.client.cursor.execute("DELETE FROM memes WHERE Name=?", (name,))
                        self.client.database.commit()
                        await self.client.say("Meme `" + name + "` removed.")
            else:
                await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))
        




def setup(client):
    client.add_cog(Memes(client))