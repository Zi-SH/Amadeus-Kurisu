from discord.ext import commands
import sqlite3
import modules.responses
import modules.utils
import discord


async def sendnews(client, context, newsgroup, news):
                        print(newsgroup)
                        if newsgroup.lower() != "emergency" and newsgroup.lower() != "allservers":
                            client.cursor.execute("SELECT * FROM newsgroups WHERE Shortname=?", (newsgroup,))
                            newsgroup_query = client.cursor.fetchone()

                            if newsgroup_query != None:
                                client.cursor.execute("SELECT * FROM newssenders WHERE NewsgroupShorthand=? AND UserID=?", (newsgroup_query[1],int(context.message.author.id)))
                                newssender_query = client.cursor.fetchone()
                                if newssender_query != None or context.message.author.id == client.config.owner_id:
                                    print(newsgroup)
                                    client.cursor.execute("SELECT * FROM newsgroupsubscriptions WHERE NewsgroupShorthand=?", (newsgroup_query[1],))
                                    query_results = client.cursor.fetchall()
                                    print(query_results)
                                    for result in query_results:
                                        print(result)
                                        server = discord.utils.find(lambda r: r.id == str(result[0]), client.servers)
                                        print(server)
                                        if server != None:
                                            newschannel = modules.utils.fetch_news_channel(server, client.cursor, context)
                                            print(newschannel.name + " " + newschannel.id)
                                            await client.send_message(newschannel, news)
                        else:
                            print("reached all servers")
                            for server in client.servers:
                                print(server.name)
                                newschannel = modules.utils.fetch_news_channel(server, client.cursor, context)
                                print(newschannel.name + " " + newschannel.id)
                                await client.send_message(newschannel, news)
                                



class Announce:
    """This is the Development module of Amadeus-Kurisu. This module contains all of the general purpose development commands for Amadeus-Kurisu."""

    def __init__(self, client):
        self.client = client
        self.utils = modules.utils
        self.responses = modules.responses

        print("Addon \"{}\" loaded.".format(self.__class__.__name__))

        self.client.cursor.execute('''CREATE TABLE IF NOT EXISTS newsgroups (
            Name varchar(255),
            Shortname varchar(255),
            Description varchar(280)
            )
            ''')
        
        self.client.cursor.execute('''CREATE TABLE IF NOT EXISTS newsgroupsubscriptions (
            ServerID int, 
            ServerName varchar(255),
            NewsgroupName varchar(255),
            NewsgroupShorthand varchar(255)
            )
            ''')
        self.client.cursor.execute('''CREATE TABLE IF NOT EXISTS newssenders (
            UserID int, 
            UserName varchar(255),
            NewsgroupName varchar(255),
            NewsgroupShorthand varchar(255)
            )
            ''')

        self.client.database.commit()
            
        
    





    @commands.group(pass_context=True)
    async def news(self, context):
        return "TBI"

    @commands.group(pass_context=True)
    async def newsgroup(self, context):
        return "TBI"



    @news.command(pass_context=True, name="announce", aliases=["send"])
    async def news_sendnews(self, context, newsgroup, news):
        await sendnews(self.client, context, newsgroup, news)

    #@commands.command(pass_context=True, name="announce", aliases ="sendnews")
    
    @newsgroup.command(pass_context=True, name="announce", aliases=["send"])
    async def newsgroup_sendnews(self, context, newsgroup, news):
        await sendnews(self.client,context,newsgroup,news)

    @newsgroup.command(pass_context=True, aliases=["add"])
    async def create(self, context, name, shortname, description):
        if context.message.author.id == self.client.config.owner_id:
                self.client.cursor.execute("SELECT * FROM newsgroups WHERE Shortname=?", (shortname,)) # Check if there's one with said shortname already.
                newsgroup_obj = self.client.cursor.fetchone()

                if newsgroup_obj is not None:
                    await self.client.say("Newsgroup with shortname `" + shortname + "` already exists!")
                else:
                    self.client.cursor.execute("INSERT INTO newsgroups VALUES (?,?,?)", (name, shortname,description))
                    self.client.database.commit()
                    await self.client.say("Newsgroup `" + name + "` created with shortname of `" + shortname + "`.")

        else:
            await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))


    @newsgroup.command(pass_context=True, name="list")
    async def list_newsgroups(self, context):
        self.client.cursor.execute("SELECT * FROM newsgroups")
        newsgroups = self.client.cursor.fetchall()
        newsgroup_string = """"""
        for newsgroup in newsgroups:
            newsgroup_string += "Name: " +  newsgroup[0] + "          Shortname: " + newsgroup[1] + " \n"
        

        await self.client.say("```List of Newsgroups: \n" + newsgroup_string + "```")


    @newsgroup.command(pass_context=True, aliases=["information"])
    async def info(self, context, shortname):
        self.client.cursor.execute("SELECT * FROM newsgroups WHERE Shortname=?", (shortname,))

        newsgroup = self.client.cursor.fetchone()

        if newsgroup is None:
            await self.client.say("Newsgroup with shortname `" + shortname + "` doesn't exist.")
        else:
            desc_shorthand = "Shortname: " + newsgroup[1]
            embed = discord.Embed(title=newsgroup[0], description=desc_shorthand, colour=0xDEADBF)
            embed.set_author(name="Newsgroup", icon_url="https://cdn.discordapp.com/attachments/128190937464700928/341665245875535872/Channel-Icon.png")
            embed.add_field(name="Description: ", value=newsgroup[2], inline=True)
            await self.client.say(embed=embed)


    @newsgroup.command(pass_context=True, aliases=["delete"])
    async def remove(self, context, shortname):
            #print(self.client.config.owner_id)
            if context.message.author.id == self.client.config.owner_id:
                args_used = len(context.message.content.split("remove ")[1].split(" "))
                print(args_used)
                if args_used is not 1 and " " not in name:
                    await self.client.say(self.utils.randomize_string(self.responses.invalid_argument_count, {"%name%": context.message.author.mention, "%arguments_required%": "1", "%arguments_used%": str(args_used)}))
                else: 
                    #print(name)
                    self.client.cursor.execute("SELECT * FROM newsgroups WHERE Shortname=?", (shortname,))

                    remove_query = self.client.cursor.fetchone()

                    

                    if remove_query is None:
                        await self.client.say("Newsgroup with shortname `" + shortname + "` doesn't exist.")
                    else:
                        self.client.cursor.execute("SELECT * FROM newsgroupsubscriptions WHERE NewsgroupShorthand=?", (shortname,))
                        query_results = self.client.cursor.fetchall()
                        print(query_results)
                        for result in query_results:
                            print(result)
                            server = discord.utils.find(lambda r: r.id == str(result[0]), self.client.servers)
                            print(server)
                            if server != None:
                                newschannel = self.utils.fetch_news_channel(server, self.client.cursor, context)
                                print(newschannel.name + " " + newschannel.id)
                                await self.client.send_message(newschannel, "This server has been unsubscribed from the newsgroup \"" + remove_query[0] + "\" as the newsgroup has been removed.")

                        #await self.client.say("Logic for this coming soon. Name: " + str(name) + " URL: " + str(url))
                        #print(query)
                        self.client.cursor.execute("DELETE FROM newsgroups WHERE Shortname=?", (shortname,))
                        self.client.cursor.execute("DELETE FROM newsgroupsubscriptions WHERE NewsgroupShorthand=?", (shortname,))
                        self.client.database.commit()
                        await self.client.say("Newsgroup `" + remove_query[0] + "` with shortname `" + shortname + "` removed and all servers unsubscribed as per cleanup.")
            else:
                await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))

    @newsgroup.command(pass_context=True)
    async def subscribe(self, context, shortname):
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='BotCommanderRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        commander_id = self.client.cursor.fetchone()[3]
        commander_role = discord.utils.find(lambda r: r.id == commander_id, context.message.author.roles)
        print(commander_role)
        if commander_role != None or context.message.author.server_permissions.manage_server or context.message.author.server_permissions.administrator:
            #Check if server is already subscribed to the shorthand. 
            self.client.cursor.execute("SELECT * FROM newsgroups WHERE Shortname=?", (shortname,))
            result_exists = self.client.cursor.fetchone()
            if result_exists != None:
                self.client.cursor.execute("SELECT * FROM newsgroupsubscriptions WHERE NewsgroupShorthand=? AND ServerID=?", (shortname,context.message.server.id,))
                result_subscribed = self.client.cursor.fetchone()
                if result_subscribed != None:
                    await self.client.say("This server is already subscribed to the newsgroup `" + result_exists[0] + "`.")
                else:
                    self.client.cursor.execute("INSERT INTO newsgroupsubscriptions VALUES (?,?,?,?)", (context.message.server.id, context.message.server.name, result_exists[0],result_exists[1],))
                    self.client.database.commit()
                    await self.client.say("This server has been subscribed to the newsgroup `" + result_exists[0] + "`.")
        else:
            await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))

        #return "TBI"

    @newsgroup.command(pass_context=True)
    async def unsubscribe(self, context, shortname):
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='BotCommanderRoleID' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        commander_id = self.client.cursor.fetchone()[3]
        commander_role = discord.utils.find(lambda r: r.id == commander_id, context.message.author.roles)
        print(commander_role)
        if commander_role != None or context.message.author.server_permissions.manage_server or context.message.author.server_permissions.administrator:
            #Check if shorthand exists
            self.client.cursor.execute("SELECT * FROM newsgroups WHERE Shortname=?", (shortname,))
            result_exists = self.client.cursor.fetchone()
            if result_exists != None:
                #If it exists, check if server is subscribed.
                self.client.cursor.execute("SELECT * FROM newsgroupsubscriptions WHERE NewsgroupShorthand=? AND ServerID=?", (shortname,context.message.server.id,))
                result_subscribed = self.client.cursor.fetchone()
                if result_subscribed == None:
                    await self.client.say("This server isn't subscribed to the newsgroup `" + result_exists[0] + "`!")
                else:
                    self.client.cursor.execute("DELETE FROM newsgroupsubscriptions WHERE NewsgroupShorthand=?", (shortname,))
                    self.client.database.commit()
                    await self.client.say("Server has been unsubscribed from the newsgroup `" + result_exists[0] + "`.")
            else:
                await self.client.say("This newsgroup doesn't exist!")
        else:
            await self.client.say(self.utils.randomize_string(self.responses.no_permission_insults, {"%name%": context.message.author.mention}))



        
    @newsgroup.command(pass_context=True)
    async def subscriptions(self, context):
        self.client.cursor.execute("SELECT * FROM newsgroupsubscriptions")
        subscriptions = self.client.cursor.fetchall()
        newsgroup_string = """"""
        for subscription in subscriptions:
            self.client.cursor.execute("SELECT * FROM newsgroups WHERE Shortname=?", (subscription[3],))
            newsgroup = self.client.cursor.fetchone()
            if newsgroup != None:
                newsgroup_string += "Name: " +  newsgroup[0] + "          Shortname: " + newsgroup[1] + " \n"
        

        await self.client.say("```" + context.message.server.name + " is subscribed to the following newsgroups: \n" + newsgroup_string + "```")


        

def setup(client):
    client.add_cog(Announce(client))