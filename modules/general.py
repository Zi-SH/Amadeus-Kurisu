from discord.ext import commands
import sqlite3
import discord
import modules.utils
import modules.responses
import datetime
from random import randrange


class General:
    """This is the General module of Amadeus-Kurisu. This module contains all of the general purpose commands for Amadeus-Kurisu."""

    def __init__(self, client):
        self.client = client
        self.utils = modules.utils
        self.responses = modules.responses
        print("Addon \"{}\" loaded.".format(self.__class__.__name__))
        
    @commands.command(pass_context=True)
    async def ping(self, context):
        """Pong!"""
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='ping' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        #print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            self.client.cursor.execute("SELECT * FROM config WHERE Setting='PingMessage' AND ServerID=?", (context.message.channel.server.id,))
            #c.execute("SELECT * FROM config WHERE Setting='PingMessage' AND ServerID='123123123123'")
            custom_message = self.client.cursor.fetchone()
            print(custom_message)
            if custom_message is not None and custom_message[3] is not None:
                await self.client.say(custom_message[3])
            elif custom_message is None:
                await self.client.say("Pong!")
            else:
                await self.client.say("Pong!")

    @commands.command(pass_context=True) 
    async def pinquote(self, context, channel=None, pin_id=None):
        """
        \"There are most likely a lot of embarrassing stuff from me in there. Feels bad, man.\"

        Returns a random pin if no arguments given. 
        Returns a random pin from a channel if first argument is given
        Returns a specific pin from a specific channel if both arguments are given.

        Command can be disabled.
        """
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='pinquote' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        ##print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled" :
            print("Command Disabled")
        else:
            channel_obj = None if channel is None else self.client.get_channel(channel[2:-1])
            await self.pin_embed(context=context, pin_id=pin_id, channel=channel_obj)

    @commands.command(pass_context=True, name="8ball", aliases=["willofsg", "sgwill"])
    async def eight_ball(self, context):
        """Eh? Who believes 8balls anyway? It's an utterly unscientific concept. Jeez.
    
        Command can be disabled.
        """
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='8ball' AND ServerID=?", (context.message.channel.server.id,))
        disabled_state = self.client.cursor.fetchone()
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            await self.client.say(self.utils.randomize_string(self.responses.eight_ball_responses))
    
    
    @commands.command(pass_context="True", name="server",aliases=["serverinfo", "guildinfo"])
    async def serverinfo(self, context):
        """I should probably be able to fetch info from @channel anyway-----.
        
        Fetches the info of the server (or "guild" as Discord likes to call them).
        
        Command can be disabled."""
        self.client.cursor.execute("SELECT * from config WHERE Setting='serverinfo' AND ServerID=?", (context.message.channel.server.id,))
        disabled_state = self.client.cursor.fetchone()
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            #A literal copypaste of the logic from the old AmaKuri Implementation should do with added support for disabling the command.
            #print(message.channel.server.created_at)
            embed = discord.Embed(title="Server Info", description="", colour=0xDEADBF)
            embed.set_author(name=context.message.channel.server.name, icon_url=context.message.channel.server.icon_url)
            embed.set_image(url=context.message.channel.server.icon_url)
            embed.add_field(name="Created on: ", value=context.message.channel.server.created_at.strftime("%c"), inline=True)
            embed.add_field(name="Users on Server:", value=context.message.channel.server.member_count, inline=True)
            embed.add_field(name="Server Owner: ", value=context.message.channel.server.owner.display_name, inline=True)
            if context.message.channel.server.default_channel is not None:
                embed.add_field(name="Default Channel: ", value=context.message.channel.server.default_channel.name, inline=True)
            embed.add_field(name="Server Region: ", value=str(context.message.channel.server.region), inline=True)
            embed.add_field(name="Verification Level: ", value=str(context.message.channel.server.verification_level), inline=True)
            embed.add_field(name="Role Count: ", value=len(context.message.channel.server.roles), inline=True)
            embed.add_field(name="Emoji Count: ", value=len(context.message.channel.server.emojis), inline=True)
            embed.add_field(name="Channel Count: ", value=len(context.message.channel.server.channels), inline=True)

            await self.client.say(embed=embed)

    async def pin_embed(self, context, channel=None, pin_id=None):

        if pin_id is not None:
            pin_id = int(pin_id) - 1

        if channel is None:
            pins = await self.client.pins_from(context.message.channel)
        else:
            pins = await self.client.pins_from(channel)

        if type(pin_id) is int:
            i = pin_id
        else:
            i = randrange(0, len(pins))

        if not pins[i].attachments:
            pins[i].attachments = [{'url': ""}]


        try:
            author = "{} ({})".format(pins[i].author, pins[i].author.display_name) \
                if pins[i].author.nick \
                else "{}".format(pins[i].author)
            pin_embed = discord.Embed(color=pins[i].author.color)
        except AttributeError as e:
            print("Caught Error: " + str(e))
            print("This is most likely because the user has left the server")
            author = "{}".format(pins[i].author)
            pin_embed = discord.Embed(color=discord.Color.default())

        if pins[i].content:
            pin_embed.add_field(name="Content:", value=pins[i].content, inline=False)

        pin_embed.set_image(url=str(pins[i].attachments[0]['url']))
        pin_embed.set_author(name=author, icon_url=pins[i].author.avatar_url)
        await self.client.say(embed=pin_embed)
        print("Done")


    


def setup(client):
    client.add_cog(General(client))
