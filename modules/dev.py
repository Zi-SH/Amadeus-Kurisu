from discord.ext import commands
import sqlite3

class Development:
    """This is the Development module of Amadeus-Kurisu. This module contains all of the general purpose development commands for Amadeus-Kurisu."""

    def __init__(self, client):
        self.client = client
        print("Addon \"{}\" loaded.".format(self.__class__.__name__))
        
    @commands.command(pass_context=True, name="perms")
    async def check_perms(self, context):
        """Prints the Server Permissions for the sender to console.
        
        Command can be disabled."""
        self.client.cursor.execute("SELECT * FROM config WHERE Setting='perms' AND ServerID=?", (context.message.channel.server.id,)) # Fetch Bot Commander Role ID.
        disabled_state = self.client.cursor.fetchone()
        #print(disabled_state[3])
        if disabled_state is not None and disabled_state[3] == "Disabled":
            print("Command Disabled")
        else:
            print(context.message.author.server_permissions)
        

def setup(client):
    client.add_cog(Development(client))