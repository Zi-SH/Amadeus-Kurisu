**Amadeus-Kurisu**

A Discord-bot made in Discord.py, based off the fictional character, Makise Kurisu.
The bot is currently in a pre-alpha stage, following a recode of previous legacy code.
The previous release was under closed source, but the changelog can be viewed here: http://davixxa.ml/KurisuChangelog.php

**Features Include:**

* The bot is modular, using the popular cogs system in Discord.py.
* Most commands can be individually disabled and enabled on a per-server basis¨
* The bot stores settings and other data in a database running on SQLite3
* All settings can be changed on a per-server basis.
* Text-chat mute feature work in progress
* A simple permissions system

**Prerequisites**

* Python 3.5
* Discord.py

**Installation**

1. Install Python 3.5. This can be done from Python.org on Windows, and using apt-get on Debian-based systems.
2. Run `pip install discord.py[voice]` on Windows, or `pip3 install discord.py[voice]` on Mac or Linux based systems.
3. Download the bot from the repository
4. Create a bot account in the Discord developer control panel. 
5. Create a new file called `config.py` in the main directory.
6. Add the following code:
```
token = "" #This token should be displayed in your Discord bot control panel.
owner_id = "" #Use your Discord account's ID, fetched by doing: "\@your_user_name"
command_prefix = "¤" #Change this for another command prefix
```
7. Run it using `python main.py` on Windows, or `python3 main.py` on Mac or Linux based systems.


**Usage**

The command-prefix by default is currently `¤`. This will change to `!` in future releases. It currently might be unsupported, depending on the current development of the bot.